from django.db import models
from django.utils import timezone
from django.conf import settings


# Create your models here.
class Receipt(models.Model):
    vendor = models.CharField("Vendor", max_length=200)
    total = models.DecimalField("Total", max_digits=10, decimal_places=3)
    tax = models.DecimalField("Tax", max_digits=10, decimal_places=3)
    date = models.DateTimeField(
        "Date", default=timezone.now, null=True, blank=True
    )
    purchaser = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="receipts",
        on_delete=models.CASCADE,
    )
    category = models.ForeignKey(
        "ExpenseCategory",
        related_name="receipts",
        on_delete=models.CASCADE,
    )
    account = models.ForeignKey(
        "Account",
        related_name="receipts",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.vendor


class ExpenseCategory(models.Model):
    name = models.CharField("Name", max_length=50)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="categories",
        on_delete=models.CASCADE,
    )
    def __str__(self):
        return self.name


class Account(models.Model):
    name = models.CharField("Name", max_length=100)
    number = models.CharField(max_length=20)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="accounts",
        on_delete=models.CASCADE,
    )
    def __str__(self):
        return self.name
